=== Bitcoin/Lightning Payment Gateway - LNBits ===
Contributors: philosopher_phaedrus
Requires at least: 5.1
Tested up to: 5.8
Requires PHP: 7.0
Stable tag: 0.0.2
License: MIT
License URI: https://gitlab.com/sovereign-individuals/lnbits-for-woocommerce/-/raw/main/LICENSE
Tags: bitcoin, lightning, lnbits, lightning network, accept bitcoin, accept lightning, instant bitcoin, bitcoin processor, bitcoin gateway, payment gateway, payment module, bitcoin module, bitcoin woocommerce, btc

Accept Bitcoin on your WooCommerce store, instantly over Lightning, and without extra fees.

== Description ==

Accept Bitcoin on your WooCommerce store, instantly over Lightning, and without extra fees.


== Issues and Development ==

If you find a bug, or have an idea for improvement, please [file an issue](https://gitlab.com/soverign-individuals/lnbits-for-woocommerce/-/issues/new) or send a pull request.

If you need help, you can reach out over Telegram: [https://t.me/p_phaedrus](https://t.me/p_phaedrus).


== External services used by this plugin ==

= LNBits =
This plugin uses LNBits instance (lnbits.com by default, or your own) for creating the invoice.
https://lnbits.org/

= Blockchain.info =
This plugin uses Blockchain.info for converting fiat currencies into Bitcoin, according to the current rates.
https://www.blockchain.com/api

= Google Charts =
This plugin uses Google Charts API for generating the QR code.
https://developers.google.com/chart


== Donation ==

If you find this plugin useful and would like to donate few sats to support the development, [send some using LNBits](https://lnbits.com/paywall/YHNaeBc4nG2U4u6zyoHmjv)!
